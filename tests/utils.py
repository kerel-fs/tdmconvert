def assert_string(str1, str2, ignore=[]):
    '''
    Assert equality of two strings, one line at a time.
    Skip equality check for lines which contain a keyword given via `ignore`.

    Copyright (c) 2018 Jules David
    Source: https://github.com/galactics/beyond/blob/0.7.4/tests/io/ccsds/conftest.py
    License: MIT
    '''

    str1 = str1.splitlines()
    str2 = str2.splitlines()

    assert len(str1) == len(str2)

    if isinstance(ignore, str):
        ignore = [ignore]

    for i, (r, t) in enumerate(zip(str1, str2)):
        _ignore = False

        for ig in ignore:
            if ig in r:
                _ignore = True
                break
        if _ignore:
            continue

        assert r == t, f"Line {i}:\n{r}\n{t}\n"
