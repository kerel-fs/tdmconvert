"""
This module provide the celery tasks of the ingestion service.
"""
from celery import Celery
from celery.utils.log import get_task_logger
from prometheus_client import Counter, Gauge, Summary

import settings
from ingestion_utils import ForwardingError, get_tdms_from_seesat
from prometheus_utils import PG_REGISTRY as reg
from prometheus_utils import push_metrics

APP = Celery()
APP.config_from_object('settings', namespace='CELERY')

LOGGER = get_task_logger(__name__)


@APP.task
def get_allowlist():
    """
    This task returns the allowlist for submitters to seesat-l.
    """
    return settings.SUBMITTER_ALLOWLIST


SEESAT_INGEST_DURATION = Summary('seesat_ingest_job_duration_seconds',
                                 'Time speint on the seesat ingest job',
                                 registry=reg)
SEESAT_INGEST_MESSAGES = Counter(
    'seesat_ingest_messages_total',
    'Count of email messages from allowlisted submitters seen during seesat ingest.',
    registry=reg)
SEESAT_INGEST_DATASETS = Counter(
    'seesat_ingest_datasets_total',
    'Count of datasets (message, norad id, site id pairs) seen during seesat ingest.',
    registry=reg)
SEESAT_INGEST_MEASUREMENTS = Counter('seesat_ingest_measurements_total',
                                     'Count of (IOD) measurements seen during seesat ingest.',
                                     registry=reg)
SEESAT_INGEST_LAST_SUCCESS = Gauge('seesat_ingest_last_success_unixtime',
                                   'Last time the seesat ingest job successfully finished',
                                   registry=reg)
SEESAT_INGEST_FAILED_TASKS = Counter('seesat_ingest_failed_tasks_total',
                                     'Count of failed ingest tasks',
                                     registry=reg)


@APP.task
def ingest_seesat():
    """
    This task fetches latest messages from the seesat-l mailing list
    from the configured IMAP server.
    """

    with SEESAT_INGEST_DURATION.time():
        try:
            stats = get_tdms_from_seesat(logger=LOGGER)

            SEESAT_INGEST_MESSAGES.inc(stats['messages'])
            SEESAT_INGEST_DATASETS.inc(stats['datasets'])
            SEESAT_INGEST_MEASUREMENTS.inc(stats['measurements'])
            SEESAT_INGEST_LAST_SUCCESS.set_to_current_time()
        except ForwardingError as err:
            print(err)
            SEESAT_INGEST_FAILED_TASKS.inc(1)

    push_metrics(registry=reg)


APP.conf.beat_schedule = {
    'ingest-seesat-every-10-minutes': {
        'task': 'app.ingest_seesat',
        'schedule': 600.0,
    },
}
APP.conf.timezone = 'UTC'
