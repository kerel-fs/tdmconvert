#!/usr/bin/env python3

import re
import sys

import requests

url = 'http://localhost:5000/analyse'
filename = '../EON_Opt02/test_eon_opt2/prototype_input.kvn'
params = {
    'period_max': '2.0',
    'period_min': '0.5',
    'period_step': '0.01',
    'fap_limit': '0.001',
    'long_period_peak_ratio': '0.9',
    'cleaning_max_power_ratio': '0.2',
    'cleaning_alliase_proximity_ratio': '0.2',
    'pdm_bins': '20',
    'half_window': '10',
    'poly_deg': '1',
    'limit_to_single_winow': '5',
    'single_window_poly_deg': '2',
}

if __name__ == '__main__':
    with open(filename, 'rb') as input_file:
        try:
            files = {'file': input_file}
            response = requests.post(url, data=params, files=files)
            response.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print(f'HTTPError: {err}')
            sys.exit(1)

    response_filename = re.findall("filename=(.+)", response.headers['content-disposition'])[0]

    print(f'Content-Length: {response.headers["content-length"]}')
    print(f'Filename: `{response_filename}`, but using `output.zip` instead.')

    with open('output.zip', 'wb') as fp_out:
        fp_out.write(response.content)
