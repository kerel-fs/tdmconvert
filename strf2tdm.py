#!/usr/bin/env python3

from tdmconvert import strf2tdm

if __name__ == '__main__':
    strf2tdm(doppler_filename='tests/data/strf_5304653.dat',
             output_filename='out0.xml',
             tdm_format='kvn',
             obs_metadata={'satellite_id': 'EQAP-0335-6677-5204-0983',
                           'frequency_tx': 435502923.076,
                           'ground_station_id': 2012},
             originator='LSF')
    # 'ccsds_tdm_5304653.xml'
