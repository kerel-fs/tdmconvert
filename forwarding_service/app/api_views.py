"""
This module provides the Forwarding Service API views with the following HTTP REST API endpoint:
- POST `/forward_tdms`
"""
# - Pylint doesn't dsicover app.webdav_utils (but it works when deployed)
# pylint: disable=import-error
import json
import os
import tempfile
import zipfile
from datetime import datetime
from http import HTTPStatus

import requests
from eon_opt02 import LightCurve
from flask import Blueprint, Response, jsonify, request, send_file

import app.settings as settings

BP = Blueprint("api", __name__, url_prefix="")


@BP.route("/")
def hello_world():
    """
    This dummy endpoint returns just a dummy message and serves testing purposes only.
    """
    return jsonify(hello="world")


@BP.route("/forward_tdms", methods=["PUT"])
def forward_tdms():
    """
    This endpoint accepts a set of CCSDS TDM messages and forwards them via WebDAV.
    """
    data = json.loads(request.data)

    print(f"Received {len(data['tdms'])} TDM message(s), forwarding...")
    try:
        session = requests.Session()
        session.auth = (settings.WEBDAV_USERNAME, settings.WEBDAV_PASSWORD)

        for filename, data in data["tdms"].items():
            print(f"- {filename}")
            response = session.put(f"{settings.WEBDAV_URL}/{filename}", data=data)
            response.raise_for_status()

        print("Done.")
        return Response(json.dumps({"message": "Success"}), mimetype="application/json")
    except requests.exceptions.RequestException as err:
        print(f"Upload failed with {err}")
        return Response(
            json.dumps({
                "message": "Error",
                "details": str(err)
            }),
            mimetype="application/json",
            status=HTTPStatus.INTERNAL_SERVER_ERROR,
        )


EON_OPT02_ANALYSIS_PARAMS_NAMES = [
    "period_max",
    "period_min",
    "period_step",
    "fap_limit",
    "long_period_peak_ratio",
    "cleaning_max_power_ratio",
    "cleaning_alliase_proximity_ratio",
    "pdm_bins",
    "half_window",
    "poly_deg",
    "limit_to_single_winow",
    "single_window_poly_deg",
]
OUTPUT_FILENAME = 'lightcurve_{timestamp:%Y-%m-%d_T%H%M%S}'
DEBUG_FILE_HANDLING = False


@BP.route("/analyse", methods=["POST"])
def analyse():
    """Performs analysis of an uploaded lightcurve and returns the results in a zip file"""

    if "file" not in request.files:
        response = {"status": "error", "message": "No file uploaded"}
        return jsonify(response), 400

    input_kvn = request.files["file"]
    # kvm_filename = os.path.splitext(kvm.filename)[0]

    if not input_kvn.filename.endswith(".kvn"):
        response = {"status": "error", "message": "Invalid file type (expected kvn extension)"}
        return jsonify(response), 400

    with tempfile.TemporaryDirectory() as tempdir_name:
        # Save input lightcurve in kvn format
        # Note: Input filename is re-used by eon02_opt.LightCurve for the output filename
        input_filename = os.path.join(tempdir_name,
                                      OUTPUT_FILENAME.format(timestamp=datetime.now()) + '.kvn')
        input_kvn.save(input_filename)

        # Parse input parameters
        try:
            analysis_params = {
                key: float(request.form.get(key)) if "." in request.form.get(key) else int(
                    request.form.get(key))
                for key in EON_OPT02_ANALYSIS_PARAMS_NAMES if key in request.form
            }
        except ValueError as err:
            error_value = str(err).split("'")[1]
            response = {
                "status": "error",
                "message": f"Error during parameter parsing. Invalid value: '{error_value}'"
            }
            return jsonify(response), 400

        if not DEBUG_FILE_HANDLING:
            # Perform lightcurve analysis
            lightcurve = LightCurve(input_filename)
            lightcurve.analyse(**analysis_params, export=tempdir_name)
        else:
            with open(os.path.join(tempdir_name, 'example.txt'), 'wt') as fp_example:
                fp_example.write('This is an example file\n')

        # Remove input data to prevent accidentially returning it to the user
        os.remove(input_filename)

        # Prepare output zip file
        with tempfile.NamedTemporaryFile('w+b') as fp_zip_file:
            output_filenames = os.listdir(tempdir_name)
            print(f'DEBUG: Output Filenames: {output_filenames}')

            if len(output_filenames) == 0:
                response = {
                    "status": "error",
                    "message": "Lightcurve analysis module did not create any output files."
                }
                return jsonify(response), 400

            output_filenames = os.listdir(tempdir_name)
            print(f'DEBUG: Output Filenames2: {output_filenames}')

            with zipfile.ZipFile(fp_zip_file.name, "w", zipfile.ZIP_DEFLATED) as zip_file:
                for filename in output_filenames:
                    zip_file.write(os.path.join(tempdir_name, filename), arcname=filename)

            response = send_file(
                fp_zip_file.name,
                mimetype="application/zip",
                as_attachment=True,
                attachment_filename='output.zip',
            )

    return response
