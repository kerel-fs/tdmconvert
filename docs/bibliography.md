# Bibliography

Open Source Python Libraries which handle CCSDS OEM and/or TDM files.

| Project Name | State | License | Remarks |
|-|-|-|-|
| [beyond](https://github.com/galactics/beyond) | active | MIT | [docs: I/O - CCSDS](https://beyond.readthedocs.io/en/stable/api/io.html#module-beyond.io.ccsds), provides interface to load & write TDMs |
| [oem](https://github.com/bradsease/oem) | active (Dec 2021) | MIT |  Python tools for working with Orbit Ephemeris Messages (OEMs) |
| [orbdetpy](https://ut-astria.github.io/orbdetpy/) | active (Oct 2021) | GPL-3 | Thin wrapper over Orekit |
| [ccsds-ndm](https://github.com/egemenimre/ccsds-ndm/) | active (Aug 2021) | GPL-3 | Does have a [Tracking Data Message API](https://ccsds-ndm.readthedocs.io/en/latest/ndmclasses2/tdm.html) |
| [ccsdspy](https://github.com/ddasilva/ccsdspy) | 2 years (Nov 2019) | BSD-3 | stalled |


(Non-exhaustive) Open Source Python Libraries which are related to CCSDS SLE, CCSDS FP and ECSS PUS.

| Project Name | State | License | Remarks |
|-|-|-|-|
| [Ccsds2CZML](https://gitlab.com/jorispio/ccsds2czml) | active (Dec 2021) | MIT | README states that TDM specifically will not be implemented. |
| [Python CFDP](https://gitlab.com/librecube/lib/python-cfdp) | active (Dec 2021) | MIT | |
| [AIT DSN](https://ait-dsn.readthedocs.io/en/latest/index.html) | active (Dec 2021) | MIT | CCSDS SLE, FDP |
| [spacepackets](https://github.com/robamu-org/py-spacepackets) | active (Jan 2022) | Apache-2 | CCSDS and ECSS packet implementations in Python |
| [puslib](https://github.com/pxntus/puslib) | active (Jan 2022) | MIT | ECSS Packet Utilization Standard implementation |
